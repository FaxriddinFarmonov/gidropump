
from django.db import models
from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin,UserManager,AbstractUser

# class Boshqaruvchi(UserManager):
#     def create_user(self, username,password,is_staff=False,is_superuser=False,is_active=True, **extra_fields):
#         user = self.model(
#             username=username,
#             password=password,
#             is_staff=is_staff,
#             is_superuser=is_superuser,
#             is_active=is_active,
#             **extra_fields
#         )
#         user.set_password(str(password))
#         user.save()
#         return user

#     def createsuper_user(self,username,password=None,**extra_fields):
#         return self.create_user(username,password,True,True)
    
    
# class User(AbstractBaseUser,PermissionsMixin):
#     username = models.CharField(max_length=50,unique=True)
#     names = models.CharField(max_length=100,blank=True,null=True)
#     phone = models.CharField(max_length=14,blank=True,null=True)

    
#     is_superuser = models.BooleanField(default=False)
#     is_staff = models.BooleanField(default=False)
#     is_active = models.BooleanField(default=True)
#     aferta = models.BooleanField(default=True)

#     USERNAME_FIELD =  'username'
#     REQUIRED_FIELDS = []






class User(AbstractUser):
    phone = models.CharField(max_length=13)





















class Province(models.Model):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title

class SubProvince(models.Model):
    title = models.CharField(max_length=255)
    province = models.ForeignKey(Province, models.CASCADE)

    def __str__(self):
        return self.title


class Pump(models.Model):
    name = models.CharField(max_length=300)
    info = models.TextField()
    StartDate = models.DateTimeField(auto_now_add=True)
    UpdateDate = models.DateTimeField(auto_now=True)
    budget = models.FloatField()
    image = models.ImageField(blank=True,null=True)
    Privacy = models.BooleanField(blank=True,null=True)
    province = models.ForeignKey(Province, models.SET_NULL, null=True, blank=True)
    subprovince = models.ForeignKey(SubProvince, models.SET_NULL, null=True, blank=True)
    phone = models.CharField(max_length=13,verbose_name='Tel nomer kiriting')
    
    def __str__(self):
        return self.name