from django.urls import path
from .views import *
from projectapp.auth.login import user_login
from projectapp.auth.logout import logout_user
from projectapp.auth.register import register_user
urlpatterns = [
   
    path('index/',index,name='index'),
    path('createpump/',create_pump,name='create_pump'),
    path('semantic/',semantik,name='semantik'),
    path('get_province',get_province,name='get_province'),
    path('malumotbir/',create_pump,name='create_pump'),
    
    path('ff/',GetProvince.as_view(),name='getprovince'),
    path('ll/',GetSubprovince.as_view(),name='province'),
    path('users/',users,name='users'),
    
    path('login/',user_login,name='user_login'),
    path('cre/<int:pk>/',getsubprovince,name='getsubprovince'),
    path('register/',register_user,name='register'),
    path('logout/',logout_user,name='logout'),


]
