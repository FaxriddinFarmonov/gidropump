from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login
from .models import Province,Pump,SubProvince
from .forms import PumpForm
from django.http import JsonResponse,HttpResponse
from django.views import View



def create_pump(request):
    form = PumpForm()
    if request.method == 'POST':
        forms = PumpForm(request.POST,request.FILES)
        if forms.is_valid():
            forms.save()
            return redirect('index')
        else:
            print(forms.errors)
    ctx = {
        'form':form
    }
    return render(request,'malumotbir.html',ctx)


def index(request):
   
    return render(request,'index.html')


def semantik(request):
    qs = Province.objects.all()
    return render(request,'semantic.html',{'qs':qs})


def get_province(request):
    qs_val = list(Province.objects.values())
    return JsonResponse({'data':qs_val})


def get__subprovince(request,*args, **kwargs):
    selected_subprovince = kwargs.get('subprovince')
    obj_models = list(SubProvince.objects.filter(sub_name=selected_subprovince).values())
    return JsonResponse({'data':obj_models})

class GetProvince(View):
    def get(self,request,*args, **kwargs):
        print(request.GET)
        province = SubProvince.objects.values('province').distinct()        
        ctx = {
            'province':province,
        }
        return render(request,'malumotbir.html',ctx)


class GetSubprovince(View):
    def get(self,request,province,*args, **kwargs):
        if request.is_ajax():
            province = SubProvince.objects.filter(province=province).values('id','title')
            return JsonResponse({'data':list(province)})
        return HttpResponse('hgdhscvhvdsw')
        
        
def users(request):
    pumps = Pump.objects.all()
    return render(request,'user.html',{'pumps':pumps})


def getsubprovince(request,pk):
    subprovince = SubProvince.objects.filter(pk=pk)
    print('ishladi')
    return JsonResponse({'data':list(subprovince)})
